package com.greatlearning.app.controller;

import java.util.Scanner;

import com.greatlearning.app.model.Employee;
import com.greatlearning.app.service.EmployeeCredentialService;

public class EmployeeController {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Employee employee = new Employee("Tarang", "Gupta");
		EmployeeCredentialService service = new EmployeeCredentialService();

		System.out.println("\n Please enter below options");
		System.out.println("\n 1. Technical, \n 2. Admin, \n 3. Human Resource, \n 4. Legal");

		int option = sc.nextInt();

		String email = null;

		switch (option) {
		case 1:
			email = service.generateEmailAddress(employee.getFirstName(), employee.getLastName(), "technical");
			service.showCredentials(employee.getFirstName(), email);

			break;
		case 2:

			email = service.generateEmailAddress(employee.getFirstName(), employee.getLastName(), "admin");
			service.showCredentials(employee.getFirstName(), email);

			break;
		case 3:

			email = service.generateEmailAddress(employee.getFirstName(), employee.getLastName(), "humanResource");
			service.showCredentials(employee.getFirstName(), email);

			break;
		case 4:

			email = service.generateEmailAddress(employee.getFirstName(), employee.getLastName(), "l1egal");
			service.showCredentials(employee.getFirstName(), email);

			break;
		default:
			System.out.println("Please enter valid option");

		}
		sc.close();
	}
}
