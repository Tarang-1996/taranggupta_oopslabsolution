package com.greatlearning.app.service;

import java.util.Random;

public class EmployeeCredentialService {

	/**
	 * to generate the password in combination of number, capital letter, small
	 * letter & special character
	 * 
	 * @return char[]
	 */
	public char[] generatePassword() {

		String capLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lowerLetters = "abcdefghijklmnopqrstuvwxyz";
		String specialChar = "!@#$%^&*_=+-/.?<>)";
		String numbers = "1234567890";
		String combined = numbers + capLetters + lowerLetters + specialChar;
		Random random = new Random();
		char[] password = new char[8];

		for (int i = 0; i < 8; i++) {
			password[i] = combined.charAt(random.nextInt(combined.length()));
		}

		return password;

	}

	/**
	 * To generate email of the employee in following format
	 * firstNamelastName@department.company.com
	 * 
	 * @param firstName 
	 * First Name of the employee
	 * 
	 * @param lastName   
	 * Last Name of the employee
	 * 
	 * @param department 
	 * department of the employee
	 * 
	 * @return String
	 */
	public String generateEmailAddress(String firstName, String lastName, String department) {

		return firstName.toLowerCase() + lastName.toLowerCase() + "@" + department.toLowerCase() + ".greatlearning.com";
	}

	/**
	 * To display the employee credentials
	 * 
	 * @param firstName 
	 * FirstName of the Employee
	 * 
	 * @param email     
	 * Email of the Employee
	 * 
	 * @return void
	 */
	public void showCredentials(String firstName, String email) {

		System.out.println("Dear " + firstName + " your generated credentials are as follows");
		System.out.println("Email --->" + email);
		System.out.println("Password --->" + String.valueOf(generatePassword()));

	}

}
